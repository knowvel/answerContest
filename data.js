export default {
	getSystem: {
		"_id": "28ee4e3e60e6565028a95a54394dcba4",
		"answer_type": {
			"list": [{
				"name": "卡片"
			}, {
				"name": "对话"
			}, {
				"name": "列表"
			}],
			"type": "卡片"
		},
		"invite": false,
		"nav": [{
			"checked": true,
			"name": "活动说明"
		}, {
			"checked": true,
			"name": "开始答题"
		}, {
			"checked": true,
			"name": "开始练习"
		}, {
			"checked": true,
			"name": "查看排行"
		}, {
			"checked": true,
			"name": "答题记录"
		}],
		"rank": [{
			"name": "今日排行",
			"checked": true
		}, {
			"checked": true,
			"name": "累计排行"
		}],
		"answer_nums": "2",
		"bg_music": {
			"is_music": true,
			"music": "https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/2021-07-08.mp3"
		},
		"index_bg_img": {
			"img": "https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/2021-06-08--1623117692573.jpg",
			"is_img": true
		},
		"answer_bg_img": {
			"is_img": false,
			"img": ""
		},
		"collect_info": [{
			"_id": "b00064a7609c7db616ecfa2d2f3eb2a9",
			"checked": true,
			"name": "编号"
		}, {
			"_id": "17453ede609c7dbd09527ac90574aa7a",
			"checked": true,
			"name": "姓名"
		}, {
			"name": "单位",
			"_id": "28ee4e3e609c7dc3182b95902a2a6c9f",
			"checked": false
		}, {
			"_id": "17453ede609c7dc909527c594fd993fe",
			"checked": false,
			"name": "电话"
		}, {
			"_id": "28ee4e3e609c7dce182b97625ff84611",
			"checked": false,
			"name": "微信"
		}, {
			"_id": "79550af2609c7dd51652ad2b4a0c4416",
			"checked": false,
			"name": "备注"
		}, {
			"_id": "b00064a760a76035194111257fb56456",
			"checked": false,
			"name": "联系人"
		}],
		"notice": {
			"content": "答题竞赛小程序发布啦~~~~",
			"is_notice": true
		},
		"theme": {
			"list": [{
				"name": "党建知识",
				"_id": "79550af260a38b4417c9df363670cdf3"
			}],
			"show": "党建知识"
		},
		"isHelp": true,
		"rank_bg_img": {
			"img": "",
			"is_img": false
		},
		"service": {
			"list": [{
				"name": "微信客服"
			}, {
				"plugid": "",
				"name": "企业微信客服"
			}],
			"who": "微信客服"
		},
		"share": {
			"is_share": true,
			"content": {
				"cover": "https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/2021-06-08--1623117692573.jpg",
				"title": "答题竞赛小程序",
				"url": "pages/index/index"
			}
		},
		"topic_nums": "15"
	},
	getRule: {
		is_show: true,
		value: "<p><strong>活动规则</strong></p><ol><li>参赛要求：每人每天答题一次，限时10分钟，随机20道题，时间到自动提交。</li><li>系统自动选取25道题。</li><li>答题时间：一周七天均可答题，每天24:00更新。</li><li>积分规则：答对一题2分，答错不扣分。</li></ol>",
		_id: "28ee4e3e60a217b8197ddd746ba5a4c2"
	},
	is_inviteCdx: {},
	getUser: {
		"_id": "79550af260e907da258ec22f58a7b197",
		"user": [{
			"_id": "b00064a7609c7db616ecfa2d2f3eb2a9",
			"checked": true,
			"title": "编号",
			"value": "BHHR"
		}, {
			"checked": true,
			"title": "姓名",
			"value": "秋兮",
			"_id": "17453ede609c7dbd09527ac90574aa7a"
		}],
		"user_id": "79550af260e8f83d258b198a704743fb",
		"answer_nums": 0,
		"help_nums": 0,
		"help_user": [],
		"id": 3
	},
	getAnswerRecord: {
		answerTime: 1626060189973,
		score: 40
	},
	getAnswerResult: {
		"answerData": [{
				"answer": [
					"C"
				],
				"is_yes": 1,
				"list": [{
						"active": true,
						"name": "保持党的优良作风",
						"num": "A"
					},
					{
						"active": false,
						"name": "促进中国经济快速发展",
						"num": "B"
					},
					{
						"active": false,
						"name": "坚持党的基本路线不动摇",
						"num": "C"
					},
					{
						"active": false,
						"name": "实践好科学发展观",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "用邓小平理论武装全党，是（）的根本保证。",
				"type": "单选",
				"value": [
					"A"
				]
			},
			{
				"_id": "b00064a760a483c818979d496caa57b9",
				"answer": [
					"C"
				],
				"is_yes": 0,
				"list": [{
						"active": false,
						"name": "打倒资本主义，建设社会主义",
						"num": "A"
					},
					{
						"active": false,
						"name": "成立中华人民共和国",
						"num": "B"
					},
					{
						"active": true,
						"name": "全心全意为人民服务",
						"num": "C"
					},
					{
						"active": false,
						"name": "完成中华民族的伟大复兴",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "中国共产党建党的目的和出发点是()。",
				"type": "单选",
				"value": [
					"C"
				]
			},
			{
				"_id": "79550af260a483ca17f19f0b0c71e0f5",
				"answer": [
					"D"
				],
				"is_yes": 1,
				"list": [{
						"active": true,
						"name": "如何建设社会主义",
						"num": "A"
					},
					{
						"active": false,
						"name": "如何巩固社会主义",
						"num": "B"
					},
					{
						"active": false,
						"name": "如何发展社会主义",
						"num": "C"
					},
					{
						"active": false,
						"name": "如何实践改革开放",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "邓小平理论第一次比较系统，比较完整地回答了在中国这样经济文化落后的国家存在一系列基本问题，其中不包括（）。",
				"type": "单选",
				"value": [
					"A"
				]
			},
			{
				"_id": "28ee4e3e60a483c819ff1dc278836a11",
				"answer": [
					"B"
				],
				"is_yes": 1,
				"list": [{
						"active": false,
						"name": "群众路线",
						"num": "A"
					},
					{
						"active": false,
						"name": "实事求是",
						"num": "B"
					},
					{
						"active": true,
						"name": "独立自主",
						"num": "C"
					},
					{
						"active": false,
						"name": "武装斗争",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "()是毛泽东思想的根本点，是党的思想路线的核心。",
				"type": "单选",
				"value": [
					"C"
				]
			},
			{
				"_id": "28ee4e3e60a483c919ff1def29ac7150",
				"answer": [
					"B"
				],
				"is_yes": 0,
				"list": [{
						"active": false,
						"name": "工人阶级",
						"num": "A"
					},
					{
						"active": true,
						"name": "广大共产党员",
						"num": "B"
					},
					{
						"active": false,
						"name": "人大代表",
						"num": "C"
					},
					{
						"active": false,
						"name": "政府官员",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "改革开放以来，（）带头解放思想，站在改革开往的前列，开拓创新，大胆创新，创出显著的成绩。",
				"type": "单选",
				"value": [
					"B"
				]
			},
			{
				"_id": "b00064a760a483ca18979d9304b3d2ac",
				"answer": [
					"B"
				],
				"is_yes": 0,
				"list": [{
						"active": false,
						"name": "党员行动标准",
						"num": "A"
					},
					{
						"active": true,
						"name": "实践标准",
						"num": "B"
					},
					{
						"active": false,
						"name": "学习标准",
						"num": "C"
					},
					{
						"active": false,
						"name": "奉献标准",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "“三个代表”重要思想集中体现了党的性质，体现了社会主义本质，它把生产力标准、社会进步标准、人民利益标准、（），有机地统一了起来。",
				"type": "单选",
				"value": [
					"B"
				]
			},
			{
				"_id": "28ee4e3e60a483c819ff1dd158abc370",
				"answer": [
					"B"
				],
				"is_yes": 0,
				"list": [{
						"active": false,
						"name": "坚持执法为公",
						"num": "A"
					},
					{
						"active": true,
						"name": "坚持执政为民",
						"num": "B"
					},
					{
						"active": false,
						"name": "坚持以人为本",
						"num": "C"
					},
					{
						"active": false,
						"name": "坚持党的先进性",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "贯彻“三个代表”的要求，本质在于()。",
				"type": "单选",
				"value": [
					"B"
				]
			},
			{
				"_id": "28ee4e3e60a483c819ff1dc75e5d900e",
				"answer": [
					"A"
				],
				"is_yes": 1,
				"list": [{
						"active": false,
						"name": "解放思想实事求是",
						"num": "A"
					},
					{
						"active": true,
						"name": "改革开放发展经济",
						"num": "B"
					},
					{
						"active": false,
						"name": "以经济建设为中心",
						"num": "C"
					},
					{
						"active": false,
						"name": "走中国特色社会主义道路",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "()是邓小平理论的精髓。",
				"type": "单选",
				"value": [
					"B"
				]
			},
			{
				"_id": "b00064a760a483c918979d6f06f476bd",
				"answer": [
					"D"
				],
				"is_yes": 1,
				"list": [{
						"active": false,
						"name": "马克思",
						"num": "A"
					},
					{
						"active": true,
						"name": "恩格斯",
						"num": "B"
					},
					{
						"active": false,
						"name": "毛泽东",
						"num": "C"
					},
					{
						"active": false,
						"name": "马克思和恩格斯",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "（）为适应无产阶级斗争的需要，总结了无产阶级斗争的经验，批判地继承了人类历史上一切先进思想的优秀成果。",
				"type": "单选",
				"value": [
					"B"
				]
			},
			{
				"_id": "28ee4e3e60a483c919ff1dfa630011b4",
				"answer": [
					"C"
				],
				"is_yes": 0,
				"list": [{
						"active": false,
						"name": "毛泽东",
						"num": "A"
					},
					{
						"active": false,
						"name": "江泽民",
						"num": "B"
					},
					{
						"active": true,
						"name": "胡锦涛",
						"num": "C"
					},
					{
						"active": false,
						"name": "邓小平",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "“任何困难都难不倒英雄的中国人民”出自谁口？()",
				"type": "单选",
				"value": [
					"C"
				]
			},
			{
				"_id": "79550af260a483c717f19ea30888c46a",
				"answer": [
					"D"
				],
				"is_yes": 1,
				"list": [{
						"active": false,
						"name": "代表中国先进生产力的发展要求",
						"num": "A"
					},
					{
						"active": true,
						"name": "代表中国先进文化的前进方向",
						"num": "B"
					},
					{
						"active": false,
						"name": "代表最广大人民的根本利益",
						"num": "C"
					},
					{
						"active": false,
						"name": "全心全意为人民服务",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "中国共产党的根本宗旨是（）。",
				"type": "单选",
				"value": [
					"B"
				]
			},
			{
				"_id": "79550af260a483c817f19ec55af0c13a",
				"answer": [
					"A"
				],
				"is_yes": 1,
				"list": [{
						"active": false,
						"name": "坚持党的先进性",
						"num": "A"
					},
					{
						"active": true,
						"name": "坚持与时俱进",
						"num": "B"
					},
					{
						"active": false,
						"name": "坚持执政为民",
						"num": "C"
					},
					{
						"active": false,
						"name": "坚持以人为本",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "全面贯彻“三个代表”思想，核心是()。",
				"type": "单选",
				"value": [
					"B"
				]
			},
			{
				"_id": "79550af260a483ca17f19ef137f0d6f8",
				"answer": [
					"A"
				],
				"is_yes": 0,
				"list": [{
						"active": true,
						"name": "马克思主义哲学",
						"num": "A"
					},
					{
						"active": false,
						"name": "马克思主义政治经济学",
						"num": "B"
					},
					{
						"active": false,
						"name": "科学社会主义",
						"num": "C"
					},
					{
						"active": false,
						"name": "以上均包括",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "（）是无产阶级的科学世界观和方法论，是无产阶级及其政党认识世界和改造世界的锐利思想武器。",
				"type": "单选",
				"value": [
					"A"
				]
			},
			{
				"_id": "cbddf0af60a483ca09890f6547013519",
				"answer": [
					"A"
				],
				"is_yes": 1,
				"list": [{
						"active": false,
						"name": "党的指导思想",
						"num": "A"
					},
					{
						"active": true,
						"name": "三个代表",
						"num": "B"
					},
					{
						"active": false,
						"name": "科学发展观",
						"num": "C"
					},
					{
						"active": false,
						"name": "中国特色社会主义理论",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "()是党的先进性建设、执政能力建设、思想建设、政治建设、组织建设和作风建设的理论基础。",
				"type": "单选",
				"value": [
					"B"
				]
			},
			{
				"_id": "79550af260a483c917f19edc67e9c6c5",
				"answer": [
					"A"
				],
				"is_yes": 1,
				"list": [{
						"active": false,
						"name": "人民子弟兵",
						"num": "A"
					},
					{
						"active": false,
						"name": "工人积极",
						"num": "B"
					},
					{
						"active": true,
						"name": "广大党员",
						"num": "C"
					},
					{
						"active": false,
						"name": "农民",
						"num": "D"
					}
				],
				"state": 1,
				"theme_id": "79550af260a38b4417c9df363670cdf3",
				"title": "（）不畏艰险，不怕疲劳，在汶川灾情的废墟前，与时间和死神赛跑，充分发挥了突击队和生力军的作用。",
				"type": "单选",
				"value": [
					"C"
				]
			}
		],
		"score": 40
	},
	getTodayRank: [],
	getAllRank: [{
			_id: 0,
			name: '单机1',
			score: 100,
			time: 60
		},
		{
			_id: 1,
			name: '单机2',
			score: 90,
			time: 70
		},
		{
			_id: 2,
			name: '单机3',
			score: 90,
			time: 80
		},

		{
			_id: 3,
			name: '单机4',
			score: 80,
			time: 32
		},
		{
			_id: 4,
			name: '单机5',
			score: 70,
			time: 43
		},
		{
			_id: 5,
			name: '单机6',
			score: 60,
			time: 10
		},

	],
	getTopic: [{
		"_id": "cbddf0af60ece49716725a553e140794",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "单选",
		"title": "要坚持无禁区.全覆盖.零容忍，坚持_______，坚持受贿行贿一起查，坚决防止党内形成利益集团。",
		"list": [{
			"name": "重预防.强高压.长震慑",
			"num": "A"
		}, {
			"name": "重遏制.强高压.长震慑",
			"num": "B"
		}, {
			"name": "重遏制.不减压.长震慑",
			"num": "C"
		}, {
			"name": "重遏制.强高压.长威慑",
			"num": "D"
		}],
		"answer": ["B"]
	}, {
		"_id": "28ee4e3e60ece49729ee763030a90e6d",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "单选",
		"title": "建设____ .____ .____. 劳动者大军.弘扬劳模精神和工匠精神，营造劳动光荣的社会风尚和精益求精的敬业风气。",
		"list": [{
			"name": "知识型.技能型.创新型",
			"num": "A"
		}, {
			"name": "创新型.技能型.知识型",
			"num": "B"
		}, {
			"name": "技能型.创新型.知识型",
			"num": "C"
		}, {
			"name": "技能型.知识型.创新型",
			"num": "D"
		}],
		"answer": ["A"]
	}, {
		"_id": "b00064a760ece49226efb6843fa005b1",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "多选",
		"title": "毛泽东思想的活的灵魂，是贯串于毛泽东思想各个部分的立场、观点和方法，它的基本方面是()",
		"list": [{
			"name": "实事求是",
			"num": "A",
			"checked": false
		}, {
			"name": "群众路线",
			"num": "B",
			"checked": false
		}, {
			"name": "独立自主",
			"num": "C",
			"checked": false
		}],
		"answer": ["ABC"]
	}, {
		"_id": "28ee4e3e60ece49629ee76003a05d58c",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "判断",
		"title": "先进性是马克思主义政党的生命所系、力量所在，要靠千千万万高素质党员来体现。",
		"list": [{
			"name": "正确",
			"num": "A"
		}, {
			"name": "错误",
			"num": "B"
		}],
		"answer": ["A"]
	}, {
		"_id": "79550af260ece4972630884a7b07a573",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "判断",
		"title": "改善民生，我们必须要优先发展农业，建设人力资源强国。",
		"list": [{
			"name": "正确",
			"num": "A"
		}, {
			"name": "错误",
			"num": "B"
		}],
		"answer": ["B"]
	}, {
		"_id": "79550af260ece4962630882233817e9a",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "判断",
		"title": "以经济建设为中心是立国之本，是我们党和国家兴旺发达、长治久安的根本要求",
		"list": [{
			"name": "正确",
			"num": "A"
		}, {
			"name": "错误",
			"num": "B"
		}],
		"answer": ["B"]
	}, {
		"_id": "79550af260ece4962630882868553a68",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "判断",
		"title": "全面贯彻党的十七大精神，高举中国特色社会主义伟大旗帜，以邓小平理论和“三个代表”重要思想为指导",
		"list": [{
			"name": "正确",
			"num": "A"
		}, {
			"name": "错误",
			"num": "B"
		}],
		"answer": ["A"]
	}, {
		"_id": "28ee4e3e60ece49629ee760e6ff6fa9b",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "判断",
		"title": "社会主义荣辱观是社会主义核心价值体系的的基础。",
		"list": [{
			"name": "正确",
			"num": "A"
		}, {
			"name": "错误",
			"num": "B"
		}],
		"answer": ["A"]
	}, {
		"_id": "79550af260ece4972630884537ffe655",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "单选",
		"title": "（）是强国之路，是我们党我们国家发展进步的活力源泉。",
		"list": [{
			"name": "改革开放",
			"num": "A"
		}, {
			"name": "四项基本原则",
			"num": "B"
		}, {
			"name": "坚持党的领导",
			"num": "C"
		}, {
			"name": "人民当家作主",
			"num": "D"
		}],
		"answer": ["A"]
	}, {
		"_id": "cbddf0af60ece49716725a7241bed1cd",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "单选",
		"title": "建设现代化经济体系，必须把发展经济的着力点放在______上，把提高供给体系质量作为主攻方向，显著增强我国经济质量优势。 ",
		"list": [{
			"name": "实体经济 ",
			"num": "A"
		}, {
			"name": "共享经济 ",
			"num": "B"
		}, {
			"name": "虚拟经济",
			"num": "C"
		}, {
			"name": "国民经济",
			"num": "D"
		}],
		"answer": ["A"]
	}, {
		"_id": "79550af260ece497263088586f531ccd",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "单选",
		"title": "中国共产党的根本组织原则是()。",
		"list": [{
			"name": "少数服从多数",
			"num": "A"
		}, {
			"name": "下级服从上级",
			"num": "B"
		}, {
			"name": "民主集中制",
			"num": "C"
		}, {
			"name": "党的代表大会制度和各级党委领导制",
			"num": "D"
		}],
		"answer": ["C"]
	}, {
		"_id": "b00064a760ece49326efb68872bacb82",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "单选",
		"title": "（）是无产阶级的科学世界观和方法论，是无产阶级及其政党认识世界和改造世界的锐利思想武器。",
		"list": [{
			"name": "马克思主义哲学",
			"num": "A"
		}, {
			"name": "马克思主义政治经济学",
			"num": "B"
		}, {
			"name": "科学社会主义",
			"num": "C"
		}, {
			"name": "以上均包括",
			"num": "D"
		}],
		"answer": ["A"]
	}, {
		"_id": "b00064a760ece49326efb69d00dc2c95",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "单选",
		"title": "党章规定，改革开放以来我们所面临的三大历史任务不包括（）。",
		"list": [{
			"name": "实现推进现代化建设",
			"num": "A"
		}, {
			"name": "完成祖国统一",
			"num": "B"
		}, {
			"name": "维护世界和平与促进共同发展",
			"num": "C"
		}, {
			"name": "建设中国特色社会主义社会",
			"num": "D"
		}],
		"answer": ["D"]
	}, {
		"_id": "79550af260ece4972630883f5fdbd316",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "多选",
		"title": "以胡锦涛为总书记的党中央，着眼全面建设小康社会的伟大实践，多次强调并深刻论述了()，把全党的认识升华到一个新的高度。",
		"list": [{
			"name": "全面、协调、可持续的发展观",
			"num": "A",
			"checked": false
		}, {
			"name": "办实事、务实效、求实绩的政绩观",
			"num": "B",
			"checked": false
		}, {
			"name": "人才资源是第一资源、人人皆可成才、人才存在于人民群众之中的人才观",
			"num": "C",
			"checked": false
		}, {
			"name": "权为民所用、情为民所系、利为民所谋的群众观",
			"num": "D",
			"checked": false
		}],
		"answer": ["ABCD"]
	}, {
		"_id": "cbddf0af60ece49716725a6116d18fa7",
		"theme_id": "79550af260a38b4417c9df363670cdf3",
		"state": 1,
		"type": "判断",
		"title": "科学发展观，是立足发展中国特色社会主义实际，总结我国发展实践，借鉴国外发展经验，适应新的发展要求提出来的",
		"list": [{
			"name": "正确",
			"num": "A"
		}, {
			"name": "错误",
			"num": "B"
		}],
		"answer": ["B"]
	}],
	getAnswer_nums: {
		"answer_nums": 0,
		"help_nums": 0,
		"help_user": []
	}
}
