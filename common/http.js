import Vue from 'vue'
import store from '../store/index.js'
import data from '../data.js'

const _this = Vue.prototype

function getData(name){
	let obj = {
		data:[]
	}

	let names
	// 需要返回id
	names = ['addUser','submitAnswerTemp','submitAnswer']
	if(names.includes(name)){
		 obj.id = true
		return obj
	}
	// 需要返回多数据
	names = ['getTodayRank','getAllRank','getTopic']
	if(names.includes(name)){
		obj.data = data[name]
		return obj
	}
	
	obj.data[0] = data[name]
	return obj
}

// 云函数请求封装
export default function $http(options) {
	return new Promise((reslove, reject) => {
		let name = options.data.action
		if (_this.$store.state.vuex_isOffline) {
		 	reslove(getData(name))
		}
		uniCloud.callFunction(options).then(res => {
			if (res.result.code == 'EXCEED_REQUEST_LIMIT' || res.result.code == 90001) {
				uni.showToast({
					title: '服务器次数已满',
					icon: 'none'
				})
			}
			reslove(res.result)
		}).catch(err => {
			if (!_this.$store.state.vuex_isOffline) {
				_this.$u.vuex('vuex_isOffline', true)
				reslove(getData(name))
			}
		})
	})
}
