// 云函数请求封装
import $http from "../http.js"
// 用户
export const user = (action,params,_id) =>{
	return $http({
		name:'user',
		data:{
			action,
			params,
			_id
		},
	})
}
