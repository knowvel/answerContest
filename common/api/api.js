// 云函数请求封装
// 批量导出文件

const requireApi = require.context(
	// 检索目录
	'.',
	// 是否检索子文件(子目录)
	false,
	// 匹配文件
	/.js$/
)

let module = {}
requireApi.keys().forEach((key,index)=>{
	if(key === './api.js') return
	Object.assign(module,requireApi(key))
})

export default module

/**
 * 调用方法
 * this.$api.get_label({}).then(res=>{})
 * 
 * 
 * 
 * */