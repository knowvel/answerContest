# 答题竞赛系统

### 介绍
本小程序主要用于答题竞赛，需要什么样的题目只需要在后台上传不同的题库即可。可用于学校、企业、相关单位等。

### 在线体验小程序
目前小程序已经发布，大家可以体验下

![](https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/小程序图.jpg)

### 联系
如果在安装过程中有什么问题,或者需要我帮忙搭建相同的答题竞赛系统，可以私信我

### 项目说明
+   本项目使用的是云开发(**腾讯云**)
+	本项目技术采用的是 **uni-app**
+	代码库中代码可以运行，以json数据(data.js)作为一些基本数据
+	控制数据线上和单机的文件在 **common/http.js** 中
+	云函数请求以进行过封装，都在 **common** 中
+	请求方式看代码

### 小程序功能截图
<img src="https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/首页.png" style="width:220px;height:420px;margin:5px;"/>
<img src="https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/活动说明.png" style="width:220px;height:420px;margin:5px;"/>
<img src="https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/邀请码.png" style="width:220px;height:420px;margin:5px;"/>
<img src="https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/答题.png" style="width:220px;height:420px;margin:5px;"/>
<img src="https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/答题结束.png" style="width:220px;height:420px;margin:5px;"/>
<img src="https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/答题记录.png" style="width:220px;height:420px;margin:5px;"/>
<img src="https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/答题详情.png" style="width:220px;height:420px;margin:5px;"/>
<img src="https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/排行.png" style="width:220px;height:420px;margin:5px;"/>


### 小程序使用过程介绍
1. 进入小程序会自动注册，生成id,并且获取系统信息，以此进行相关渲染
2. 活动说明首次请求数据后会进行存储，不会再进行数据请求
3. 练习的题目结果不会存储
4. 支持邀请码验证功能，开启后首次答题需要输入邀请码。
5. 支持设置收集用户信息，开启后，需要填写用户个人信息才能开始答题。
6. 正式答题的结果会记录在答题记录中，需要前往查看答题信息
7. 目前小程序只支持卡片式答题，后续会增加对话式和传统问卷式答题
8. 卡片模式答题过程中，单选和判断题选择答题后会自动切下一题。
9. 答题卡片前后衔接，可以快速定位到未选的卡片题目
10. 排行榜：今日排行展示的是今日所有答题次数中分数最高的那一次
11. 排行榜：累计排行由最高分数排列，如果分数持平则以答题时间少排高
12. 支持答题助力功能，开启后每天最多可以邀请5个好友进行助力（每次助力都增加一次答题次数，好友点击链接进入小程序即助力成功）
13. 更多内容需要各位慢慢体验

### 系统配置介绍
	<!-- 在 data.js 中的 getSystem 属性 -->
	
	answer_type 答题类型
	invite	邀请码
	nav	导航
	rank	排行榜
	answer_nums 每日答题次数
	bg_music	背景音乐
	index_bg_img	首页背景图
	answer_bg_img	答题背景图
	collect_info 采集用户信息
	notice	公告
	theme	活动主题
	isHelp	助力
	rank_bg_img	排行榜背景
	service	客服
	share	分享
	topic_nums 题目数量
