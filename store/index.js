import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
let lifeData = {};

let data = {
	// "answer_bg_img": {
	// 	"img": "",
	// 	"is_img": false
	// },
	// "answer_nums": "2",
	// "answer_type": {
	// 	"list": [{
	// 			"name": "卡片"
	// 		},
	// 		{
	// 			"name": "对话"
	// 		},
	// 		{
	// 			"name": "列表"
	// 		}
	// 	],
	// 	"type": "卡片"
	// },
	"bg_music": {
		"is_music": true,
		"music": "https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/2021-07-08.mp3"
	},
	// "collect_info": [{
	// 		"_id": "b00064a7609c7db616ecfa2d2f3eb2a9",
	// 		"checked": false,
	// 		"name": "编号"
	// 	},
	// 	{
	// 		"_id": "17453ede609c7dbd09527ac90574aa7a",
	// 		"checked": true,
	// 		"name": "姓名"
	// 	},
	// 	{
	// 		"_id": "28ee4e3e609c7dc3182b95902a2a6c9f",
	// 		"checked": false,
	// 		"name": "单位"
	// 	},
	// 	{
	// 		"_id": "17453ede609c7dc909527c594fd993fe",
	// 		"checked": false,
	// 		"name": "电话"
	// 	},
	// 	{
	// 		"_id": "28ee4e3e609c7dce182b97625ff84611",
	// 		"checked": true,
	// 		"name": "微信"
	// 	},
	// 	{
	// 		"_id": "79550af2609c7dd51652ad2b4a0c4416",
	// 		"checked": false,
	// 		"name": "备注"
	// 	},
	// 	{
	// 		"_id": "b00064a760a76035194111257fb56456",
	// 		"checked": false,
	// 		"name": "联系人"
	// 	}
	// ],
	"index_bg_img": {
		"img": "https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/2021-06-08--1623117692573.jpg",
		"is_img": true
	},
	"invite": true,
	"isHelp": true,
	"nav": [{
			"checked": true,
			"name": "活动说明"
		},
		{
			"checked": true,
			"name": "开始答题"
		},
		{
			"checked": true,
			"name": "开始练习"
		},
		{
			"checked": true,
			"name": "查看排行"
		},
		{
			"checked": true,
			"name": "答题记录"
		}
	],
	"notice": {
		"content": "答题竞赛小程序发布啦~~~~",
		"is_notice": true
	},
	"rank": [{
			"checked": true,
			"name": "今日排行"
		},
		{
			"checked": true,
			"name": "累计排行"
		}
	],
	// "rank_bg_img": {
	// 	"img": "",
	// 	"is_img": false
	// },
	// "service": {
	// 	"list": [{
	// 			"name": "微信客服"
	// 		},
	// 		{
	// 			"name": "企业微信客服",
	// 			"plugid": ""
	// 		}
	// 	],
	// 	"who": "微信客服"
	// },
	// "share": {
	// 	"content": {
	// 		"cover": "https://7463-tcb-q5jxa8pxdc9u1ma13328a-e9b4a9-1305862946.tcb.qcloud.la/2021-06-08--1623117692573.jpg",
	// 		"title": "答题竞赛小程序",
	// 		"url": "pages/index/index"
	// 	},
	// 	"is_share": true
	// },
	// "theme": {
	// 	"list": [{
	// 		"_id": "79550af260a38b4417c9df363670cdf3",
	// 		"name": "党建知识"
	// 	}],
	// 	"show": "党建知识"
	// },
	// "topic_nums": "15"
}

try {
	// 尝试获取本地是否存在lifeData变量，第一次启动APP时是不存在的
	lifeData = uni.getStorageSync('lifeData');
} catch (e) {

}

// 需要永久存储，且下次APP启动需要取出的，在state中的变量名
let saveStateKeys = ['vuex_system', 'vuex_user', 'vuex_invite'];
// 保存变量到本地存储中
const saveLifeData = function(key, value) {
	// 判断变量名是否在需要存储的数组中
	if (saveStateKeys.indexOf(key) != -1) {
		// 获取本地存储的lifeData对象，将变量添加到对象中
		let tmp = uni.getStorageSync('lifeData');
		// 第一次打开APP，不存在lifeData变量，故放一个{}空对象
		tmp = tmp ? tmp : {};
		tmp[key] = value;
		// 执行这一步后，所有需要存储的变量，都挂载在本地的lifeData对象中
		uni.setStorageSync('lifeData', tmp);
	}
}
const store = new Vuex.Store({

	state: {
		vuex_system: lifeData.vuex_system ? lifeData.vuex_system : data,
		vuex_user: lifeData.vuex_user ? lifeData.vuex_user : [],
		vuex_invite: lifeData.vuex_invite ? lifeData.vuex_invite : false,
		vuex_isOffline:false,
		vuex_rule:null,
	},
	mutations: {
		$uStore(state, payload) {
			// 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
			let nameArr = payload.name.split('.');
			let saveKey = '';
			let len = nameArr.length;
			if (nameArr.length >= 2) {
				let obj = state[nameArr[0]];
				for (let i = 1; i < len - 1; i++) {
					obj = obj[nameArr[i]];
				}
				obj[nameArr[len - 1]] = payload.value;
				saveKey = nameArr[0];
			} else {
				// 单层级变量，在state就是一个普通变量的情况
				state[payload.name] = payload.value;
				saveKey = payload.name;
			}
			// 保存变量到本地，见顶部函数定义
			saveLifeData(saveKey, state[saveKey])
		}
	}
})
export default store
