import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false
import api from './common/api/api.js'
Vue.prototype.$api = api

import uView from 'utils/uview-ui';
let vuexStore = require("@/store/$u.mixin.js");
import store from '@/store';
Vue.mixin(vuexStore);
App.mpType = 'app'
Vue.use(uView);
const app = new Vue({
		store,
    ...App
})
app.$mount()
